<?php

namespace ConWeb\HomepageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('HomepageBundle:Main:index.html.twig', array('name' => $name));
    }
}
